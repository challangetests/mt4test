<?php
include_once(dirname(__FILE__).'/../models/Dispositivos.php'); 

class DispositivoController{

    private $model;

    function __construct()
    {
        $this->model = new Dispositivos();
    }

    public function index(){
        $results = $this->model->index();
        require('views/dispositivos/index.php');
    }

    public function create(){
        require('views/dispositivos/create.php');
    }
    
    public function store($request){
        if($this->model->create($request))
            header('Location: /dispositivos');
    }
    
    public function edit($id){
        $dispositivo = $this->model->findById($id);
        require('views/dispositivos/edit.php');
    }

    public function update($request){
        if($this->model->update($request))
            header('Location: /dispositivos');
    }

    public function destroy($id){
        if($this->model->delete($id))
            header('Location: /dispositivos');
    }
}