<?php
include_once(dirname(__FILE__).'/../class/Hasher.php');

class HasherController{

    public function index(){
        require('views/hasher/index.php');
    }

    public function create($request){
        $hasher = new Hasher($request['string']);
        $sha512 = $hasher->sha512();
        $hmac = $hasher->hmac();
        $bcrypt = $hasher->bcrypt();
        $compare = [];
        if($request['compare']){
            $compare['sha512'] = hash_equals($request['compare'], $sha512) ? 'Igual' : "Diferente";
            $compare['hmac'] = hash_equals($request['compare'], $hmac) ? 'Igual' : "Diferente";
            $compare['bcrypt'] = hash_equals($request['compare'], $bcrypt) ? 'Igual' : "Diferente";
        }

        $results = [
            [
                "algoritmo" => 'sha512',
                "hash" => $sha512,
                "compare" => $compare ? $compare['sha512'] : '',
            ],
            [
                "algoritmo" => 'HMAC',
                "hash" => $hmac,
                "compare" => $compare ? $compare['hmac'] : '',
            ],
            [
                "algoritmo" => 'bcrypt',
                "hash" => $bcrypt,
                "compare" => $compare ? $compare['bcrypt'] : '',
            ],
        ];
        require('views/hasher/create.php');
    }

    public function compare($results){
        
    }
}