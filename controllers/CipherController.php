<?php
include_once(dirname(__FILE__).'/../class/CaesarCipher.php');
include_once(dirname(__FILE__).'/../class/SaltedAESEcrypter.php');

class CipherController{

    public function index(){
        require('views/cipher/index.php');
    }

    public function encrypt($string){
        $caesar = new CaesarCipher();
        $caesar_encrypted = $caesar->encrypt($string);

        $aes =  new SaltedAESEcrypter();
        $keys = $aes->generateKey();
        $aew_encrypted = $aes->encrypt($string, $keys['key'], $keys['iv']);
        $results = [
            "caesar" => [
                "algoritmo" => "Cifra de César",
                "encrypted" => $caesar_encrypted,
            ],
            "aes" => [
                "algoritmo" => "AES256 com SALT",
                "encrypted" => $aew_encrypted,
                "key" => base64_encode($keys['key']),
                "iv" => base64_encode($keys['iv'])
            ]
        ];
        require('views/cipher/encrypted.php');
    }

    public function decrypt($results){
        $caesar = new CaesarCipher();
        $caesar_decrypted = $caesar->decrypt($results['caesar']);

        $aes =  new SaltedAESEcrypter();
        $aew_decrypted = $aes->decrypt($results['aesEncrypt'],
            base64_decode($results['aesKey']),base64_decode($results['aesIv']));
        $decrypted = [
            "caesar" => [
                "algoritmo" => "Cifra de César",
                "decrypted" => $caesar_decrypted,
            ],
            "aes" => [
                "algoritmo" => "AES256 com SALT",
                "decrypted" => $aew_decrypted,
            ]
        ];
        require('views/cipher/decrypted.php');
    }
}