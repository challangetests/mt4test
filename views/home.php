<?php 
include(dirname(__FILE__).'/layouts/header.html'); 
?>

<div class="container mt-4">
  <div class="row">
    <div class="col-sm-12 bg-secondary">
        <div class="row mt-2">
            <div class="col-sm-6">
                <h3 class="text-light pull-left">Teste MT4</h3>
            </div>
        </div>
        <div class="row mt-2 pr-1 pl-1">
          <div class="col-sm-12">
            <a href='/dispositivos'>  
            <div class='btn btn-primary col-sm-6 offset-md-3 mb-4'>Crud de Dispositivos</div>
            </a>
            <a href='#'>  
                <button class='btn btn-primary col-sm-6 offset-md-3 mb-4' disabled>SSH Dispositivos</button>
            </a>
            <a href='/cipher'>  
                <div class='btn btn-primary col-sm-6 offset-md-3 mb-4'>Encrypter e Decrypter</div>
            </a>
            <a href='/hasher'>  
                <div class='btn btn-primary col-sm-6 offset-md-3 mb-4'>Hasher e Comparador</div>
            </a>
          </div>
        </div>
    </div>
  </div>
</div>

</body>
</html>

