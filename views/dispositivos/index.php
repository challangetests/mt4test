<?php 
include(dirname(__FILE__).'/../layouts/header.html'); 
?>

<div class="container mt-4">
  <div class="row">
    <div class="col-sm-12 bg-secondary">
        <div class="row mt-2">
            <div class="col-sm-12">
                <h3 class="text-light pull-left">Cadastro de Dispositivos</h3>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-sm-6">
                <a href='/'>
                    <div class="float-left btn btn-primary">
                        Voltar
                    </div>
                </a>
            </div>
            <div class="col-sm-6">
                <a href='/dispositivos/create'>
                    <div class="float-right btn btn-success">
                        Novo Dispositivo
                    </div>
                </a>
            </div>
        </div>
        <div class="row mt-2 pr-1 pl-1">
          <div class="col-sm-12">
            <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Hostname</th>
                        <th scope="col">IP</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Fabricante</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($results){ 
                        foreach($results as $row){ ?>
                        <tr>
                            <td><?php echo $row['id']?></td>
                            <td><?php echo $row['hostname']?></td>
                            <td><?php echo $row['ip']?></td>
                            <td><?php echo $row['tipo']?></td>
                            <td><?php echo $row['fabricante']?></td>
                            <td>
                                <a href='/dispositivos/edit?id=<?php echo $row['id'];?>'>
                                    <div class="float-right btn btn-primary">
                                        Editar
                                    </div>
                                </a>
                            </td>
                            <td>
                                <form href method='POST' action='/dispositivos/destroy'>
                                    <input type='hidden' name='id' value='<?php echo $row['id'];?>'>
                                    <button type='submit' class="float-right btn btn-danger"
                                    onclick="return confirm('Tem certeza ?');">
                                        Deletar
                                    </button>
                                </form>
                            </td>
                        </tr>
                    <?php }}?>
                </tbody>
            </table>
          </div>
        </div>
    </div>
  </div>
</div>

</body>
</html>

