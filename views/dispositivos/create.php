<?php 
include(dirname(__FILE__).'/../layouts/header.html'); 
?>

<div class="container mt-4">
  <div class="row">
    <div class="col-sm-12 bg-secondary">
        <div class="row mt-2">
            <div class="col-sm-12">
                <h3 class="text-light pull-left">Novo Dispositivo</h3>
            </div>
        </div>
        <div class="row mt-2 pr-1 pl-1">
            <div class="col-sm-12">
                <form method='POST' action='/dispositivos/store'>
                    <div class="form-group">
                        <label for="hostname">Hostname</label>
                        <input type="text" class="form-control" id="hostname" name='hostname'>
                    </div>
                    <div class="form-group">
                        <label for="ip">IP</label>
                        <input type="text" class="form-control" id="ip" name='ip'>
                    </div>
                    <div class="form-group">
                        <label for="tipo">Tipo</label>
                        <input type="text" class="form-control" id="tipo" name='tipo'>
                    </div>
                    <div class="form-group">
                        <label for="fabricante">Fabricante</label>
                        <input type="text" class="form-control" id="fabricante" name='fabricante'>
                    </div>
                    <a href='/dispositivos'>
                        <div class="btn btn-primary float-left mb-2">Voltar</div>
                    </a>
                    <button type="submit" class="btn btn-success float-right">Inserir</button>
                </form>
            </div>
        </div>
    </div>
  </div>
</div>

</body>
</html>

