<?php 
include(dirname(__FILE__).'/../layouts/header.html'); 
?>

<div class="container mt-4">
  <div class="row">
    <div class="col-sm-12 bg-secondary">
        <div class="row mt-2">
            <div class="col-sm-6">
                <h3 class="text-light pull-left">Encrypter e Decrypter</h3>
            </div>
        </div>
        <div class="row mt-2 pr-1 pl-1">
          <div class="col-sm-12">
            <form method='POST' action='/cipher/encrypt'>
                <textarea name="string" rows=3 class="form-control mb-3"></textarea>
                <div class="row">
                  <div class="col-sm-2">
                    <a href="/">
                      <div class="btn btn-primary mb-3">Voltar</div>
                    </a>
                  </div>  
                  <div class="col-sm-10">    
                    <button type="submit" class="btn btn-success float-right mb-3">Encriptar</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
    </div>
  </div>
</div>

</body>
</html>

