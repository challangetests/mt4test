<?php 
include(dirname(__FILE__).'/../layouts/header.html'); 
?>

<div class="container mt-4">
  <div class="row">
    <div class="col-sm-12 bg-secondary">
        <div class="row mt-2">
            <div class="col-sm-6">
                <h3 class="text-light pull-left">Hasher e comparador</h3>
            </div>
        </div>
        <div class="row mt-2 pr-1 pl-1">
          <div class="col-sm-12">
            <form method='POST' action='/hasher/create'>
            <div class="form-group">
                <label for="string">Texto para Hashear</label>
                <textarea name="string" id='string' rows=2 class="form-control mb-3"></textarea>
                <label for="compare">Hash de comparação</label>                
                <input type="text" class="form-control" id="compare" name='compare'>
                <div class="row">
                  <div class="col-sm-2">
                    <a href="/">
                      <div class="btn btn-primary mt-3 mb-3">Voltar</div>
                    </a>
                  </div>  
                  <div class="col-sm-10">    
                    <button type="submit" class="btn btn-success float-right mt-3 mb-3">Hashear</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
    </div>
  </div>
</div>

</body>
</html>

