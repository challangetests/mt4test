<?php 
include(dirname(__FILE__).'/../layouts/header.html'); 
?>
<div class="container mt-4">
  <div class="row">
    <div class="col-sm-12 bg-secondary">
        <div class="row mt-2">
            <div class="col-sm-6">
                <h3 class="text-light pull-left">Hasher e comparador</h3>
            </div>
        </div>
        <div class="row mt-2 pr-1 pl-1">
          <div class="col-sm-12">
            <form method='POST' action='/hasher/create'>
              <div class="form-group">
                  <label for="string">Texto para Hashear</label>
                  <textarea name="string" id='string' rows=2 class="form-control mb-3"><?php echo $request['string']?></textarea>
                  <label for="compare">Hash de comparação</label>                
                  <input type="text" class="form-control" id="compare" name='compare' value='<?php echo $request['compare']?>'>
                  <a href='/hasher'>
                    <div class="btn btn-primary float-left mt-3 mb-3">Voltar</div>
                  </a>
                  <button type="submit" class="btn btn-success float-right mt-3 mb-3">Hashear</button>
              </div>
            </form>
          </div>
        </div>
        <div class="row mt-2 pr-1 pl-1">
          <div class="col-sm-12">
            <table class="table table-striped">
              <thead class="thead-dark">
                <tr>
                  <th scope="col" style='width: 15%'>Algoritmo</th>
                  <th scope="col" style='width: 60%;'>Hash Gerada</th>
                  <th scope="col" style='width: 25%'>Comparacao</th>
                </tr>
              </thead>
              <tbody>
                <?php if($results){ 
                  foreach($results as $row){ ?>
                    <tr>
                      <td><?php echo $row['algoritmo']?></td>
                      <td>
                        <textarea disabled name="string" id='string' rows=2 class="form-control bg-secondary text-dark"><?php echo $row['hash']?></textarea>
                      </td>
                      <td><?php echo $row['compare']?></td>
                    </tr>
                <?php }}?>
              </tbody>
          </div>
        </div>
    </div>
  </div>
</div>

</body>
</html>

