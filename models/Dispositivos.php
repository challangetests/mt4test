<?php
class Dispositivos{

    private $pdo;

    function __construct()
    {
        $this->pdo = new PDO(
            'mysql:host='. getenv('MYSQL_HOST') . ';dbname='. getenv('MYSQL_DATABASE'), 
            getenv('MYSQL_USER'), getenv('MYSQL_PASSWORD')
        );
    }

    public function index(){
        $sql = 'SELECT * FROM dispositivos ORDER BY id desc';
        return $this->pdo->query($sql)->fetchAll();
    }

    public function create($fields){
        $sql = "INSERT INTO dispositivos (hostname, ip, tipo, fabricante) 
            VALUES (:hostname, :ip, :tipo, :fabricante)";
        $stmt= $this->pdo->prepare($sql);
        $stmt->execute([
            'hostname' =>$fields['hostname'], 
            'ip' => $fields['ip'], 
            'tipo' => $fields['tipo'], 
            'fabricante' => $fields['fabricante']
        ]);

        return true;
    }

    public function findById($id){
        $sql = "SELECT * FROM dispositivos where id = ?";
        $stmt= $this->pdo->prepare($sql);
        $stmt->execute([$id]);
        $dispositivo = $stmt->fetchAll();

        return $dispositivo[0];
    }

    public function update($fields){
        $sql = "UPDATE dispositivos set hostname=:hostname, ip=:ip, 
            tipo=:tipo, fabricante=:fabricante where id = :id";
        $stmt= $this->pdo->prepare($sql);
        $stmt->execute([
            'id' =>$fields['id'], 
            'hostname' =>$fields['hostname'], 
            'ip' => $fields['ip'], 
            'tipo' => $fields['tipo'], 
            'fabricante' => $fields['fabricante']
        ]);

        return true;
    }

    public function delete($id){
        $sql = "DELETE from dispositivos where id = :id";
        $stmt= $this->pdo->prepare($sql);
        $stmt->execute(['id' =>$id]);

        return true;
    }

}