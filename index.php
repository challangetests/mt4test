<?php
include_once 'class/Request.php';
include_once 'class/Router.php';
include_once 'class/DotEnv.php';
include_once 'controllers/DispositivoController.php';
include_once 'controllers/CipherController.php';
include_once 'controllers/HasherController.php';

$env = new DotEnv('.env');
$router = new Router(new Request);

$router->get('/', function() {
  require('views/home.php');
});
// tarefa 1 crud dispositivos
$router->get('/dispositivos', function($request){
  $controller = new DispositivoController();
  $controller->index();
});

$router->get('/dispositivos/create', function($request) {
  $controller = new DispositivoController();
  $controller->create();
});

$router->post('/dispositivos/store', function($request) {
  $request = $request->getBody();
  $controller = new DispositivoController();
  $controller->store($request);  
});

$router->get('/dispositivos/edit', function($request) {
  $id = $_GET['id'];
  $controller = new DispositivoController();
  $controller->edit($id);
});

$router->post('/dispositivos/update', function($request) {
  $request = $request->getBody();
  $controller = new DispositivoController();
  $controller->update($request);  
});

$router->post('/dispositivos/destroy', function($request) {
  $request = $request->getBody();
  $controller = new DispositivoController();
  $controller->destroy($request['id']);  
});

//tarefa 3 cifras
$router->get('/cipher', function($request) {
  $controller = new CipherController();
  $controller->index();
});

$router->post('/cipher/encrypt', function($request) {
  $request = $request->getBody();
  $controller = new CipherController();
  $controller->encrypt($request['string']);  
});

$router->post('/cipher/decrypt', function($request) {
  $request = $request->getBody();
  $controller = new CipherController();
  $controller->decrypt($request);  
});

//tarefa 4 hashs
$router->get('/hasher', function($request) {
  $controller = new HasherController();
  $controller->index();
});

$router->post('/hasher/create', function($request) {
  $request = $request->getBody();
  $controller = new HasherController();
  $controller->create($request);  
});