<?php
class CaesarCipher
{
    private $offset;

    function __construct($offset = 5){
        $this->offset = $offset;
    }

    public function encrypt($string){
        $encrypted_text = "";
        $offset = $this->offset % 26;
        if($offset < 0) {
            $offset += 26;
        }
        $i = 0;
        while($i < strlen($string)) {
            $c = strtoupper($string[$i]); 
            if(($c >= "A") && ($c <= 'Z')) {
                if((ord($c) + $offset) > ord("Z")) {
                    $encrypted_text .= chr(ord($c) + $offset - 26);
            } else {
                $encrypted_text .= chr(ord($c) + $offset);
            }
        } else {
            $encrypted_text .= " ";
        }
        $i++;
        }
        return $encrypted_text;
    }

    function decrypt($string) {
        $decrypted_text = "";
        $offset = $this->offset % 26;
        if($offset < 0) {
            $offset += 26;
        }
        $i = 0;
        while($i < strlen($string)) {
            $c = strtoupper($string[$i]); 
            if(($c >= "A") && ($c <= 'Z')) {
                if((ord($c) - $offset) < ord("A")) {
                    $decrypted_text .= chr(ord($c) - $offset + 26);
            } else {
                $decrypted_text .= chr(ord($c) - $offset);
            }
          } else {
              $decrypted_text .= " ";
          }
          $i++;
        }
        return $decrypted_text;
    }

    public function setOffset($offset){
        $this->offset = $offset;
    }

}