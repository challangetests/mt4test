<?php
class SaltedAESEcrypter{

    private $password;
    private $salt;
    private $keyLength;
    private $iterations;
    private $algorithm;
    private $cipher = "AES-256-CBC";

    private $key;
    private $initializationVector;

    function __construct($password = null, $salt = null, $keyLength = 32, $iterations = 1000, $algorithm =  null){
        $this->password = $password ? $password :  uniqid(mt_rand(), true);
        $this->salt = $salt ? $salt :  uniqid(mt_rand(), true);
        $this->keyLength = $keyLength;
        $this->iterations = $iterations;
        $this->algorithm = $algorithm;
    }

    public function generateKey(){
        $ivlen = openssl_cipher_iv_length($this->cipher);
        $this->initializationVector = openssl_random_pseudo_bytes($ivlen);
        $this->key = openssl_pbkdf2($this->password, $this->salt, $this->keyLength, 
            $this->iterations, $this->algorithm);
        return [
            'key' => $this->key,
            'iv' => $this->initializationVector,
        ];
    }

    public function encrypt($string, $key = null, $iv = null) {
        $key = $key ? $key : $this->key;
        $iv = $iv ? $iv : $this->initializationVector;
        $encrypted_text = base64_encode(openssl_encrypt($string, $this->cipher, $key,
            OPENSSL_RAW_DATA, $iv));
        return $encrypted_text;
    }

    public function decrypt($string, $key = null, $iv = null){
        $key = $key ? $key : $this->key;
        $iv = $iv ? $iv : $this->initializationVector;
        $decrypted_text  = openssl_decrypt(base64_decode($string), $this->cipher, $key,
            OPENSSL_RAW_DATA, $iv);
        return $decrypted_text;
    }

}