<?php
class DotEnv
{
    function __construct($path)
    {
        $env = file_get_contents($path);
        foreach(explode(PHP_EOL, $env) as $value){
            $array = explode('=', $value);
            putenv($value);
            $_ENV[$array[0]] = $array[1];
        }
    }
}