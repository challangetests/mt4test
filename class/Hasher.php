<?php 
class Hasher
{
    private $string;

    function __construct($string){
        $this->string = $string;
    }

    public function setString($string){
        $this->string = $string;
    }

    public function sha512(){
        // $salt = uniqid(mt_rand(), true);
        $salt = 'fixedsaltforhashing';
        return crypt($this->string, '$6$rounds=5000$'. $salt.'$');
    }

    public function hmac(){
        // $key = uniqid(mt_rand(), true);
        $key = 'fixedkeyforhashing';
        return hash_hmac('whirlpool', $this->string, $key);
    }

    public function bcrypt(){
        $options = [
            'cost' => 11
        ];
        return password_hash($this->string, PASSWORD_BCRYPT, $options);
    }
}