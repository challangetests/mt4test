CREATE TABLE dispositivos (
    id int NOT NULL AUTO_INCREMENT,
    hostname varchar(200),
    ip varchar(15),
    tipo varchar(100),
    fabricante varchar(200),
    PRIMARY KEY (id)
);